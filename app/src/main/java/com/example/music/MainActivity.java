package com.example.music;


import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    int[] btnIdArr = {
            0,
            R.id.btn_1,
            R.id.btn_2,
            R.id.btn_3,
            R.id.btn_4,
            R.id.btn_5,
            R.id.btn_6,
            R.id.btn_7,
    };
    final static String[] musicNumber = {
            "",
            "do",
            "re",
            "mi",
            "fa",
            "so",
            "la",
            "xi",
    };
    List<String> btnNameList = new ArrayList<>();

    {
        for (String name : musicNumber) {
            btnNameList.add(name);
        }
    }


    TextView tv_content;
    TextView tv_head;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv_content = findViewById(R.id.tv_content);
        tv_head = findViewById(R.id.tv_head);
        gen(null);//一开始就生成
    }

    int currentMusicNoIndex = 0;
    int[] currentMusicNoArr;//当前
    int multiple = 2;//两组
    int arrSize = multiple * 7;

    String[] randomMusicNumber = {
            "do",
            "re",
            "mi",
            "fa",
            "so",
            "la",
            "xi",
    };

    public void gen(View view) {
        currentMusicNoArr = new int[arrSize];
        for (int i = 0; i < arrSize; i++) {
            currentMusicNoArr[i] = i % 7 + 1;
        }
        Random random = new Random();
        for (int i = 0; i < arrSize; i++) {
            int randomInt = random.nextInt(arrSize);//[0,size)
            int tmp = currentMusicNoArr[i];
            currentMusicNoArr[i] = currentMusicNoArr[randomInt];
            currentMusicNoArr[randomInt] = tmp;
        }
        int rmn = randomMusicNumber.length;
        for (int i = 0; i < rmn; i++) {
            int randomInt = random.nextInt(rmn);//[0,rmn)
            String tmp = randomMusicNumber[i];
            randomMusicNumber[i] = randomMusicNumber[randomInt];
            randomMusicNumber[randomInt] = tmp;
        }
        //放到view视图中去
        for (int i = 0; i < randomMusicNumber.length; i++) {
            String name = randomMusicNumber[i];
            int btnId = btnIdArr[i + 1];
            Button btn = findViewById(btnId);
            btn.setText(name);
        }
        //设置值了哇
        tv_content.setText(currentMusicNoArr[0] + "");//第一个音符
        tv_head.setText(String.format("%d/%d", 1, arrSize));
        currentMusicNoIndex = 0;
    }

    public void pre(View view) {
        if (currentMusicNoIndex == 0) {//超过一定范围就停止
            Toast.makeText(this, "第一个", Toast.LENGTH_SHORT).show();
            return;
        }
        currentMusicNoIndex--;//下一个音符
        tv_content.setText(currentMusicNoArr[currentMusicNoIndex] + "");
        tv_head.setText(String.format("%d/%d", currentMusicNoIndex + 1, arrSize));
    }

    public void next(View view) {
        if (currentMusicNoIndex == arrSize - 1) {//超过一定范围就停止
            Toast.makeText(this, "结束", Toast.LENGTH_SHORT).show();
            return;
        }
        currentMusicNoIndex++;//下一个音符
        tv_content.setText(currentMusicNoArr[currentMusicNoIndex] + "");
        tv_head.setText(String.format("%d/%d", currentMusicNoIndex + 1, arrSize));
    }

    public void musicNo(View view) {
        Button btn = (Button) view;
        String text = btn.getText().toString();

        int i = btnNameList.indexOf(text);//按钮是第几个，就代表是几号音符

        if (currentMusicNoArr[currentMusicNoIndex] != i) {
            Toast.makeText(this, "不是这个音符", Toast.LENGTH_SHORT).show();
            return;
        }
        if (currentMusicNoIndex == arrSize - 1) {//超过一定范围就停止
            Toast.makeText(this, "结束", Toast.LENGTH_SHORT).show();
            return;
        }
        currentMusicNoIndex++;//下一个音符
        tv_content.setText(currentMusicNoArr[currentMusicNoIndex] + "");
        tv_head.setText(String.format("%d/%d", currentMusicNoIndex + 1, arrSize));
    }
}
